import com.google.gson.Gson;
import net.lingala.zip4j.exception.ZipException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

/**
 * Převede všchny jednotlivé jízdní řády ve formátu xml stažené z ftp://ftp.cisjr.cz/draha/celostatni/szdc/ na JSON pole obsahující kompletní název vlaku, např.: rj 72 Vindobona, Os 18001, ...
 */
public class Main {
    public static final String GVD_NAME = "GVD2020";
    public static final String ZIP_URL = "ftp://ftp.cisjr.cz/draha/celostatni/szdc/2020/" + GVD_NAME + ".ZIP";
    public static final File ZIP_FILE = new File("gvd/gvd.zip");

    public static HashMap<Integer, String> typeDictionary = new HashMap<>() {{
        put(50,"EC");
        put(63,"IC");
        put(69,"Ex");
        put(70,"EN");
        put(84,"Os");
        put(94,"SC");
        put(122,"Sp");
        put(157,"R");
        put(209,"rj");
        put(9000,"Rx");
        put(9001,"TLX");
        put(9002,"TL");
        put(9003,"LE");
        put(9004,"RJ");
        put(9005,"AEx");
        put(9006,"NJ");
        put(9007,"LET");
    }};

    public static void main(String[] args) throws ParserConfigurationException, IOException, SAXException {
        downloadGVDFiles();
        unzipFile();
        createGVDList();
        cleanUp();
    }

    public static void downloadGVDFiles() throws IOException {
        System.out.println("Downloading GVD files..");
        URL website = new URL(ZIP_URL);

        ZIP_FILE.getParentFile().mkdirs();

        ReadableByteChannel rbc = Channels.newChannel(website.openStream());
        FileOutputStream fos = new FileOutputStream(ZIP_FILE);

        fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
        System.out.println("Download complete..");
    }

    public static void createGVDList() throws IOException, ParserConfigurationException, SAXException {
        System.out.println("Parsing list..");
        File folder = new File("gvd\\" + GVD_NAME);
        List<String> outList = new ArrayList<>();
        Map<Integer, String> outMap = new TreeMap<>();

        FileOutputStream fOutNames = new FileOutputStream("knownTrainsNames.json");
        PrintStream outNames = new PrintStream(fOutNames);

        FileOutputStream fOut = new FileOutputStream("knownTrains.json");
        PrintStream out = new PrintStream(fOut);

        for (File file : folder.listFiles()) {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();

            Document document = builder.parse(file);

            Element root = document.getDocumentElement();

            Integer trainType = Integer.parseInt(root.getElementsByTagName("CommercialTrafficType").item(0).getTextContent());
            Integer trainNumber = Integer.parseInt(root.getElementsByTagName("OperationalTrainNumber").item(0).getTextContent());
            //String trainSpecificName = root.getElementsByTagName("NetworkSpecificParameter").item(0).getChildNodes().item(3).getTextContent();
            String trainToken = typeDictionary.get(trainType) + " " + trainNumber;// + " " + trainSpecificName;
            if (outList.stream().noneMatch(s -> s.equals(trainToken))) {
                outList.add(trainToken);
                outMap.put(trainNumber, typeDictionary.get(trainType));
            }
        }

        System.out.println("Parsing complete, outputting files..");

        Gson gson = new Gson();
        outNames.println(gson.toJson(outList));
        out.println(gson.toJson(outMap));

        System.out.println("Output complete..");
        //gson.toJson(outList, new FileWriter("output.json"));
    }

    public static void unzipFile() throws ZipException {
        System.out.println("Unzipping files..");
        net.lingala.zip4j.ZipFile zipFile = new net.lingala.zip4j.ZipFile(ZIP_FILE);
        zipFile.extractAll(ZIP_FILE.getParent());
        System.out.println("Unzipping complete..");
    }

    public static void cleanUp() throws IOException {
        System.out.println("Cleaning up..");
        Files.walk(ZIP_FILE.getParentFile().toPath())
                .sorted(Comparator.reverseOrder())
                .map(Path::toFile)
                .forEach(File::delete);
        System.out.println("Clean up complete..");
    }
}
